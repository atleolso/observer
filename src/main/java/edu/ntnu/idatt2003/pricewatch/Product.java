package edu.ntnu.idatt2003.pricewatch;

import java.math.BigDecimal;
import java.util.Objects;

public class Product {

    private final String sku;
    private final String brand;
    private final String name;
    private BigDecimal price;

    public Product(String sku, String brand, String name, BigDecimal price) {
        if (sku == null || brand == null || name == null || price == null) {
            throw new IllegalArgumentException("Product cannot have null values");
        }
        if (sku.isBlank() || brand.isBlank() || name.isBlank()) {
            throw new IllegalArgumentException("Product cannot have empty values");
        }
        if (price.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Price cannot be negative");
        }

        this.sku = sku;
        this.brand = brand;
        this.name = name;
        this.price = price;
    }

    public Product(Product product) {
        this(product.getSku(), product.getBrand(), product.getName(), product.getPrice());
    }

    public String getSku() {
        return sku;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        if (price == null || price.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Price cannot be null or negative");
        }
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(sku, product.sku);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sku);
    }
}
