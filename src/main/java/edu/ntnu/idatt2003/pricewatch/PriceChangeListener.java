package edu.ntnu.idatt2003.pricewatch;

import java.math.BigDecimal;

public interface PriceChangeListener {

    void priceChanged(Product product, BigDecimal oldPrice, BigDecimal newPrice);
}
