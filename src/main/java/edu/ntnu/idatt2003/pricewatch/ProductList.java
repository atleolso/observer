package edu.ntnu.idatt2003.pricewatch;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProductList extends ObservableProductList {

    private final List<Product> products;

    public ProductList() {
        products = new ArrayList<>();
    }

    public void add(Product product) {
        products.add(product);
    }

    public boolean remove(String sku) {
        return products.removeIf(product -> product.getSku().equals(sku));
    }

    public Product get(String sku) {
         return new Product(findFirst(sku));
    }

    // change price of product and notify listeners
    public void changePrice(String sku, BigDecimal newPrice) {
        Product product = findFirst(sku);
        notifyPriceChangeListeners(product, product.getPrice(), newPrice);
        product.setPrice(newPrice);
    }

    private Product findFirst(String sku) {
        Product product = products.stream().filter(p -> p.getSku().equals(sku)).findFirst().orElse(null);
        if (product == null) {
            throw new NoSuchElementException("A product with SKU " + sku + " does not exist");
        }
        return product;
    }


}
