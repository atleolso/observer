package edu.ntnu.idatt2003.pricewatch;

import java.math.BigDecimal;

public class GameConsolePriceChangeListener implements PriceChangeListener {

    // this listener is interested in price changes on game consoles
    @Override
    public void priceChanged(Product product, BigDecimal oldPrice, BigDecimal newPrice) {
        if (isConsole(product)) {
            System.out.println("ConsolePriceChangeListener: " + product.getName() + " has a new price of " + newPrice);
        }
    }

    private boolean isConsole(Product product) {
        String[] searchTerms = {"xbox", "playstation", "switch"};
        boolean productIsConsole = false;

        for (int i=0; i<searchTerms.length; i++) {
            String productName = product.getName().toLowerCase();
            if (productName.contains(searchTerms[i])) {
                productIsConsole = true;
                break;
            }
        }
        return productIsConsole;
    }
}
