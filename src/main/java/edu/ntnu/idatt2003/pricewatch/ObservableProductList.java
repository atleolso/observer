package edu.ntnu.idatt2003.pricewatch;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class ObservableProductList {

    List<PriceChangeListener> priceChangeListeners;

    public ObservableProductList() {
        priceChangeListeners = new ArrayList<>();
    }

    public void addListener(PriceChangeListener priceChangeListener) {
        priceChangeListeners.add(priceChangeListener);
    }

    public void removeListener(PriceChangeListener priceChangeListener) {
        priceChangeListeners.remove(priceChangeListener);
    }

    public void notifyPriceChangeListeners(Product product, BigDecimal oldPrice, BigDecimal newPrice) {
        priceChangeListeners.forEach(priceChangeListener -> priceChangeListener.priceChanged(product, oldPrice, newPrice));
    }

}
