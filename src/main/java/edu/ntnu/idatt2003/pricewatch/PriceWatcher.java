package edu.ntnu.idatt2003.pricewatch;

import java.math.BigDecimal;

public class PriceWatcher {

    public static void main(String... args) {
        ProductList productList = new ProductList();
        productList.add(new Product("856600", "Apple", "MacBook Pro 16 M4 Pro 48/512GB (sølv)", new BigDecimal("42990.00")));
        productList.add(new Product("336729", "Nintendo", "Nintendo Switch OLED gamingkonsoll", new BigDecimal("4395.00")));
        productList.add(new Product("218667", "Microsoft", "Xbox Series X 1 TB (sort)", new BigDecimal("6799.00")));

        PriceChangeListener bargainPriceChangeListener = new BargainPriceChangeListener();
        productList.addListener(bargainPriceChangeListener);

        PriceChangeListener consolePriceChangeListener = new GameConsolePriceChangeListener();
        productList.addListener(consolePriceChangeListener);

        productList.changePrice("856600", new BigDecimal("38990.00")); // price cut, not console
        productList.changePrice("336729", new BigDecimal("3990.00")); // price cut, console
        productList.changePrice("218667", new BigDecimal("7099.00")); // price increase, console

        productList.removeListener(bargainPriceChangeListener);
        productList.removeListener(consolePriceChangeListener);
    }
}
