package edu.ntnu.idatt2003.pricewatch;

import java.math.BigDecimal;

public class BargainPriceChangeListener implements PriceChangeListener {

    // this listener is interested in all price reductions
    @Override
    public void priceChanged(Product product, BigDecimal oldPrice, BigDecimal newPrice) {
        if (newPrice.compareTo(oldPrice) < 0 ) {
            //new price is lower than old price: it´s a bargain
            System.out.println("BargainPriceChangeListener: Price cut of " + oldPrice.subtract(newPrice) + " on " + product.getName());
        }
    }
}
