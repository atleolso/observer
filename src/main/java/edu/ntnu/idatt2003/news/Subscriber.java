package edu.ntnu.idatt2003.news;

public interface Subscriber {

    void update(Article article);
}
