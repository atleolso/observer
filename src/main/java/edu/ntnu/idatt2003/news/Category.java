package edu.ntnu.idatt2003.news;

import java.util.Random;

public enum Category {

    POLITICS,
    BUSINESS,
    ENTERTAINMENT,
    SPORTS;

    public static Category getRandom() {
        Random random = new Random();
        int index = random.nextInt(values().length);
        return values()[index];
    }

}
