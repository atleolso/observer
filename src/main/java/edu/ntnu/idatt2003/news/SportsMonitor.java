package edu.ntnu.idatt2003.news;

public class SportsMonitor implements Subscriber {

    public SportsMonitor() {
    }

    @Override
    public void update(Article article) {
        if (article.getCategory() == Category.SPORTS) {
            System.out.println("New in sports: " + article.getTitle());
        }
    }

    public static void main(String... args) {
        NewsAgency newsAgency = new NewsAgency();
        SportsMonitor sportsMonitor = new SportsMonitor();

        //subscribe to sports related news from news agency
        newsAgency.addSubscriber(sportsMonitor);
        newsAgency.produceOneArticleEverySecond(10);
        newsAgency.removeSubscriber(sportsMonitor);
    }

}
