package edu.ntnu.idatt2003.news;

public class NewsAgency extends Publisher {

    public NewsAgency() {
    }

    //produces one dummy article every second and notifies subscribers
    public void produceOneArticleEverySecond(int numberOfArticles) {
        for (int i=1; i<= numberOfArticles; i++) {
            try {
                Thread.sleep(1000);
                Article article = new Article("Article number " + i, "Lorem ipsum...", Category.getRandom());
                publish(article);
            } catch (InterruptedException e) {
                // handle exception...
            }
        }
    }
}
