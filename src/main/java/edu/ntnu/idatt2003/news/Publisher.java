package edu.ntnu.idatt2003.news;

import java.util.ArrayList;
import java.util.List;

public abstract class Publisher {

    List<Subscriber> subscribers;

    public Publisher() {
        subscribers = new ArrayList<>();
    }

    public void addSubscriber(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public void removeSubscriber(Subscriber subscriber) {
        subscribers.remove(subscriber);
    }

    public void publish(Article article) {
        subscribers.forEach(subscriber -> subscriber.update(article));
    }
}
