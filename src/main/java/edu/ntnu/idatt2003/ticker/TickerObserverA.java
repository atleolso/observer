package edu.ntnu.idatt2003.ticker;

public class TickerObserverA implements Observer {

    private final Ticker ticker;

    public TickerObserverA(Ticker ticker) {
        this.ticker = ticker;
    }

    @Override
    public void update() {
        System.out.println("Observer A (" + ticker.getNumberOfTicks() + "): sum of positive integers is " +
                sumOfN(ticker.getNumberOfTicks()));
    }

    // sum of all integers between 1 and number
    public int sumOfN(int number) {
        return (number*(number+1))/2;
    }
}
