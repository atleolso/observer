package edu.ntnu.idatt2003.ticker;

public interface Observer {

    void update();
}
