package edu.ntnu.idatt2003.ticker;

public class TickerObserverB implements Observer {

    private final Ticker ticker;

    public TickerObserverB(Ticker ticker) {
        this.ticker = ticker;
    }

    @Override
    public void update() {
        System.out.println("Observer B (" + ticker.getNumberOfTicks() + "): factorial is " + factorial(ticker.getNumberOfTicks()));
    }

    // factorial, number! (fakultet)
    private int factorial(int number) {
        return number == 1 ? 1 : number * factorial(number-1);
    }
}
