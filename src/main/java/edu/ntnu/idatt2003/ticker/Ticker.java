package edu.ntnu.idatt2003.ticker;

public class Ticker extends Subject {

    private int numberOfTicks;

    public Ticker() {
        numberOfTicks = 0;
    }

    public void run(int maxTicks) {
        while (numberOfTicks < maxTicks) {
            try {
                Thread.sleep(1000);
                numberOfTicks++;
                notifyObservers();
            } catch (InterruptedException e) {
                // handle exception...
            }
        }
    }

    public int getNumberOfTicks() {
        return numberOfTicks;
    }
}
