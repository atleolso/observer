package edu.ntnu.idatt2003.ticker;

public class TickerApp {

    public static void main(String... args) {
        Ticker ticker = new Ticker();
        TickerObserverA tickerObserverA = new TickerObserverA(ticker);
        TickerObserverB tickerObserverB = new TickerObserverB(ticker);

        ticker.attach(tickerObserverA);
        ticker.attach(tickerObserverB);

        ticker.run(10);

        ticker.detach(tickerObserverA);
        ticker.detach(tickerObserverB);
    }
}
