# observer

Code examples demonstrating the observer pattern in Java:

- Ticker: Subject `Ticker` updates its state – `numberOfTicks` – once every second. Two observers listen for changes:

  1. `TickerObserverA` prints the sum of all positive integers up to and including numberOfTicks
  2. `TickerObserverB` prints the factorial of numberOfTicks

- Pricewatch: An observable `ProductList` notifies its observers when the price of a product changes:

  1. `BargainPriceChangeListener` prints a product if the new price is lower than the old
  2. `GameConsolePriceChangeListener` prints price changes for game consoles

- News: Publisher `NewsAgency` notifies subscribers when a new article is published. Subscriber `SportsMonitor` prints sports articles.

  

